package com.hashengineering.crypto;

import com.google.bitcoin.core.Sha256Hash;
import com.google.bitcoin.core.Sha512Hash;
import fr.cryptohash.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 3S
 */
public class Threes {

    private static final Logger log = LoggerFactory.getLogger(Threes.class);
    private static boolean native_library_loaded = false;

    static {

        try {
            System.loadLibrary("onecoin_3s");
            native_library_loaded = true;
        }
        catch(UnsatisfiedLinkError x)
        {
			log.info("libonecoin_3s.so LinkError: "+x.toString());
			native_library_loaded = false;
        }
        catch(Exception e)
        {
			log.info("libonecoin_3s.so exception: "+e.toString());
            native_library_loaded = false;
        }

		if (native_library_loaded)
		{
			System.out.println("libonecoin_3s.so native library loaded");
			log.info("libonecoin_3s.so native library loaded");
		}
		else
		{
			System.out.println("libonecoin_3s.so native library failed to load, falling back to java 3S implentation");
			log.info("libonecoin_3s.so native library failed to load, falling back to java 3S implentation");
		}
	}

	
    public static byte[] threesDigest(byte[] input, int offset, int length)
    {
        byte [] buf = new byte[length];
        for(int i = 0; i < length; ++i)
        {
            buf[i] = input[offset + i];
        }
        return threesDigest(buf);
    }

    public static byte[] threesDigest(byte[] input) {
        //long start = System.currentTimeMillis();
        try {
            return native_library_loaded ? threes_native(input) : threes(input);
            /*long start = System.currentTimeMillis();
            byte [] result = x11_native(input);
            long end1 = System.currentTimeMillis();
            byte [] result2 = x11(input);
            long end2 = System.currentTimeMillis();
            log.info("x11: native {} / java {}", end1-start, end2-end1);
            return result;*/
        } catch (Exception e) {
            return null;	
        }
        finally {
            //long time = System.currentTimeMillis()-start;
            //log.info("X11 Hash time: {} ms per block", time);
        }
    }

    static native byte [] threes_native(byte [] input);


    static byte [] threes(byte header[])
    {
        //Initialize
        Sha512Hash[] hash = new Sha512Hash[3];

		//Run the chain of algorithms
        
        SHAvite512 shavite = new SHAvite512();
        hash[0] = new Sha512Hash(shavite.digest(header));

        SIMD512 simd = new SIMD512();
        hash[1] = new Sha512Hash(simd.digest(hash[0].getBytes()));

		Skein512 skein = new Skein512();
        hash[2] = new Sha512Hash(skein.digest(hash[1].getBytes()));

        return hash[2].trim256().getBytes();
    }
}
