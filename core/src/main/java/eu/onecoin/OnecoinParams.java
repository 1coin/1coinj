/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.onecoin;

import com.google.bitcoin.core.*;
import com.google.bitcoin.script.Script;
import com.google.bitcoin.script.ScriptOpCodes;
import org.spongycastle.util.encoders.Hex;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;

import static com.google.common.base.Preconditions.checkState;
import static com.hashengineering.crypto.Threes.threesDigest;

/**
 * Parameters for the testnet, a separate public instance of Bitcoin that has relaxed rules suitable for development
 * and testing of applications and new Bitcoin versions.
 */
public class OnecoinParams extends NetworkParameters {
    public OnecoinParams() {
        super();
        id = "org.onecoin.production";
        proofOfWorkLimit = Utils.decodeCompactBits(0x1e0fffffL);
        addressHeader = 115;
        acceptableAddressCodes = new int[] { 115 };
        port = 1998;
        packetMagic = 0xc8c9cacbL;
        dumpedPrivateKeyHeader = 128 + addressHeader;

        targetTimespan = (int)(4 * 60);
        interval = targetTimespan/((int)(2 * 60));

        genesisBlock.setDifficultyTarget(0x1e0ffff0L);
        genesisBlock.setTime(1414192737L);
        genesisBlock.setNonce(8749289L);
        genesisBlock.removeTransaction(0);
        Transaction t = new Transaction(this);
        try {
            //   "25-10-14 - 1Coin Launch"
            byte[] bytes = Hex.decode
					("04FFFF001D01041732352D31302D3134202D2031436F696E204C61756E6368");
            t.addInput(new TransactionInput(this, t, bytes));
            ByteArrayOutputStream scriptPubKeyBytes = new ByteArrayOutputStream();
            
            t.addOutput(new TransactionOutput(this, t, Utils.toNanoCoins(15,15), scriptPubKeyBytes.toByteArray()));
        } catch (Exception e) {
            // Cannot happen.
            throw new RuntimeException(e);
        }
        genesisBlock.addTransaction(t);
        String genesisHash = genesisBlock.getHashAsString();
        checkState(genesisHash.equals("0000016f19db4d1547cfd745da70ac908f92bf2d400f022630732aff9cf50684"),
                genesisBlock);

        subsidyDecreaseBlockCount = 680000;

        dnsSeeds = new String[] {
                "dnsseed.1coin.eu",
        };
    }

    private static BigInteger MAX_MONEY = Utils.COIN.multiply(BigInteger.valueOf(21000000));
    @Override
    public BigInteger getMaxMoney() { return MAX_MONEY; }

    private static OnecoinParams instance;
    public static synchronized OnecoinParams get() {
        if (instance == null) {
            instance = new OnecoinParams();
        }
        return instance;
    }

    /** The number of previous blocks to look at when calculating the next Block's difficulty */
    @Override
    public int getRetargetBlockCount(StoredBlock cursor) {
        if (cursor.getHeight() + 1 != getInterval()) {
            //Logger.getLogger("wallet_ltc").info("Normal ONE retarget");
            return getInterval();
        } else {
            //Logger.getLogger("wallet_ltc").info("Genesis ONE retarget");
            return getInterval() - 1;
        }
    }

    @Override public String getURIScheme() { return "onecoin:"; }

    /** Gets the hash of the given block for the purpose of checking its PoW */
    public Sha256Hash calculateBlockPoWHash(Block b) {
        byte[] blockHeader = b.cloneAsHeader().bitcoinSerialize();
        try {
            return new Sha256Hash(Utils.reverseBytes(threesDigest(blockHeader)));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    static {
        NetworkParameters.registerParams(get());
        NetworkParameters.PROTOCOL_VERSION = 70002;
    }
}
