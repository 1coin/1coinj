#ifndef HASHBLOCK_H
#define HASHBLOCK_H

#include "uint256.h"
#include "sph_skein.h"
#include "sph_shavite.h"
#include "sph_simd.h"

#ifndef QT_NO_DEBUG
#include <string>
#endif

#ifdef GLOBALDEFINED
#define GLOBAL
#else
#define GLOBAL extern
#endif

GLOBAL sph_skein512_context     z_skein;
GLOBAL sph_shavite512_context   z_shavite;
GLOBAL sph_simd512_context      z_simd;

#define fillz() do { \
    sph_skein512_init(&z_skein); \
    sph_shavite512_init(&z_shavite); \
    sph_simd512_init(&z_simd); \
} while (0) 


#define ZSKEIN (memcpy(&ctx_skein, &z_skein, sizeof(z_skein)))

template<typename T1>
inline uint256 threesHash(const T1 pbegin, const T1 pend)

{
	sph_shavite512_context     ctx_shavite;
	sph_simd512_context     ctx_simd;
	sph_skein512_context     ctx_skein;

    
	uint512 hash;

	sph_shavite512_init(&ctx_shavite);
	sph_shavite512(&ctx_shavite, (unsigned char*)&pbegin[0], (pend - pbegin) * sizeof(pbegin[0]));
    sph_shavite512_close(&ctx_shavite, (unsigned char*)&hash);

    sph_simd512_init(&ctx_simd);
    sph_simd512(&ctx_simd, (unsigned char*)&hash, sizeof(hash));
    sph_simd512_close(&ctx_simd, (unsigned char*)&hash);

	sph_skein512_init(&ctx_skein);
    sph_skein512(&ctx_skein, (unsigned char*)&hash, sizeof(hash));
    sph_skein512_close(&ctx_skein, (unsigned char*)&hash);

    return hash.trim256();
}






#endif // HASHBLOCK_H
